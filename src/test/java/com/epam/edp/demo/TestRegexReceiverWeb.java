package com.epam.edp.demo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestRegexReceiverWeb {

    private static final String[] addresses = {"autocode.epam.com", "https://google.com/", "http://epam.com", "https://yandex.ru", "http://google.com.ua"};
    private static final String[] nonAddresses = {"autocode@epam.com", "example2@example.com", "@example.com", "", "2", "ftp://google.com"};

    @Test
    public void isRegexShort() {
        assertTrue(RegexReceiver.WEB_ADDRESS_PATTERN_STRING.length() < 20);
    }

    @Test
    public void isWebAddressTest() {

        for (String address : addresses) {
            assertTrue(RegexReceiver.isWebAddress(address));
        }

        for (String nonAddress : nonAddresses) {
            assertFalse(RegexReceiver.isWebAddress(nonAddress));
        }
    }

}
